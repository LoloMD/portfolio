FROM node:latest

COPY id_rsa_home /root/.ssh/id_rsa_home

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

CMD ["npm", "start"]